import { FlowRouter } from "meteor/kadira:flow-router";
import { BlazeLayout } from "meteor/kadira:blaze-layout";

import './lib/router.js';
// Write your package code here!

// Variables exported by this module can be imported by other packages and
// applications. See reporting-tests.js for an example of importing.
export const name = 'reporting';