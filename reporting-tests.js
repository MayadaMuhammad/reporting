// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by reporting.js.
import { name as packageName } from "meteor/byte:reporting";

// Write your tests here!
// Here is an example.
Tinytest.add('reporting - example', function (test) {
  test.equal(packageName, "reporting");
});
